"""Utility functions for stock price predictor."""
import os

import numpy as np
import pandas as pd
import tensorflow as tf
from matplotlib import pyplot as plt


def load_training_data(training_data_file, scaler):
    """Preprocess training data set and return training data set and scaled
    training data.

    This function loads the training data from the specified CSV file, extracts
    the "Open" stock price to use as training data, scales the training data,
    and returns both the training data set and the scaled training data.
    :param string training_data_file: Training data file (CSV).
    :param scaler: Scaler used to normalise the training data.
    :return DataFrame dataset_training: Training data set.
    :return array training_data_scaled: Scaled training data set column vector,
    shape [n_samples, n_features=1] containing scaled "Open" stock prices.
    """
    dataset_training = pd.read_csv(training_data_file)
    print('Training data set:')
    print(dataset_training.head())
    print(dataset_training.describe())
    # Predictions will be made on the "Open" stock price; extract the column as
    # column vector [n_samples, n_features=1] as required for normalisation
    training_data = dataset_training.iloc[:, 1:2].values
    print('\nOpen stock price training data:')
    print(training_data)
    # Perform feature scaling by normalising the data
    training_data_scaled = scaler.fit_transform(training_data)
    print('\nScaled training data:')
    print(training_data_scaled)
    return dataset_training, training_data_scaled


def training_features_labels(training_data_scaled, n_features):
    """Return the training feature matrix and label array.

    This function transforms the scaled training data set into the input
    feature matrix and the labels vector, which are used to train the model.
    It runs a window of length n_features over the data set which is
    incremented by one data point throughout the data set to produce a feature
    matrix with number of features equal to n_features, and a corresponding
    label consisting of the next adjacent data point whose index is n_features.
    Each row of the feature matrix is effectively shifted left by one data
    point relative to the row above.
    :param array training_data_scaled: Scaled training data array, shape
    [n_samples, 1].
    :param int n_features: Number of desired features in the training features
    matrix.
    :return array X_train: Training feature matrix, shape
    [n_samples - n_features, n_features, 1] 3D matrix required by RNN.
    :return array y_train: Training labels vector, shape
    [n_samples - n_features,].
    """
    X_train = []
    y_train = []
    # Iterate through the training data set and produce feature matrix and
    # labels array
    for i in range(n_features, training_data_scaled.shape[0]):
        X_train.append(training_data_scaled[i - n_features:i, 0])
        y_train.append(training_data_scaled[i, 0])
    # Convert list of arrays to NumPy arrays
    X_train, y_train = np.array(X_train), np.array(y_train)
    # Reshape the training feature matrix into 3D tensor required by RNN model
    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
    print('\nTraining features:')
    print(X_train)
    print('\nTraining labels:')
    print(y_train)
    return X_train, y_train


def rnn_model(X_train, y_train, num_units, num_epochs, batch_size, model_file,
              dropout=None):
    """Return a fitted model, either from the specified file or fit model.

    This function returns a 3 hidden layer RNN model from the specified model
    file, if it exists, otherwise it creates, compiles and trains the RNN
    architecture, and saves the complete model.  The optimiser is Adam and the
    loss function is MSE.
    :param array X_train: Training feature matrix.
    :param array y_train: Training labels vector.
    :param int num_units: Number of units (neurons) in hidden layers.
    :param int num_epochs: Number of training epochs.
    :param int batch_size: Training batch size.
    :param string model_file: Model file name to load from or save to the
    complete trained model.
    :param float dropout: Dropout regularisation [0.0 .. 1.0] on input and all
    hidden layers.
    :return model: Complete trained model.
    """
    # Check whether a trained model exists
    if not os.path.exists(model_file):
        # Create, compile, train and save entire RNN model
        print(f'Model file {model_file} not found; creating and training ...')

        # Model hyperparameters
        hidden_layers = 3

        # Create and compile the RNN architecture
        model = tf.keras.Sequential()
        # Input layer
        model.add(
            tf.keras.layers.LSTM(units=num_units,
                                 return_sequences=True,
                                 input_shape=(X_train.shape[1], 1)))
        if dropout is not None:
            model.add(tf.keras.layers.Dropout(dropout))

        # Hidden layers
        for _ in range(hidden_layers - 1):
            model.add(tf.keras.layers.LSTM(units=num_units,
                                           return_sequences=True))
            if dropout is not None:
                model.add(tf.keras.layers.Dropout(dropout))

        # Final hidden layer (LSTM)
        model.add(tf.keras.layers.LSTM(units=num_units))
        if dropout is not None:
            model.add(tf.keras.layers.Dropout(dropout))

        # Output layer (fully connected)
        model.add(tf.keras.layers.Dense(units=1))

        # Compile the RNN
        model.compile(optimizer='adam', loss='mean_squared_error')

        # Train the RNN
        history = model.fit(X_train, y_train, epochs=num_epochs,
                            batch_size=batch_size)

        # Plot model training history and save to file
        pd.DataFrame(history.history).plot()
        plt.grid(True)
        plt.title('Model Training History')
        plt.show()
        plt.savefig(model_file.split('.')[0] + '.png')

        # Save the entire model in HDF5 format
        print('Saving entire model as', model_file)
        model.save(model_file)
    else:
        # Load model from file
        print(f'Model file {model_file} found; loading model ...')
        model = tf.keras.models.load_model(model_file)
    return model


def test_features_labels(test_data_file, dataset_training, scaler, n_features):
    """Return the test features and labels.

    This function returns the test features and labels by combining the
    training and test data sets, and scaling by normalising the test data.  It
    loads the test data from the specified file and extracts the actual stock
    prices as the test data labels (y_test).  It concatenates the training data
    set with the test data set because n_features previous instances are needed
    in order to get the stock price for each day, therefore both training and
    test data is needed.  It reshapes and scales the combined data into the
    test data feature matrix.
    :param string test_data_file: File name of test data file (CSV).
    :param DataFrame dataset_training: Training data set (unscaled).
    :param scaler: Scaler used to normalise the test data.
    :param int n_features: Number of desired features in the test features
    matrix.
    :return array X_test: Test feature matrix (scaled & normalised values)
    shape [n_samples(train), n_features, 1] 3D matrix required by RNN.
    :return array y_test: Test label vector (raw values) shape [n_samples,].
    """
    # Load and process the test data (considered as actual data here)
    dataset_testing = pd.read_csv(test_data_file)
    print('\nTest (actual) data set:')
    print(dataset_testing.head())
    print(dataset_testing.describe())
    # y_test, test labels for "Open" stock price; extract the column as column
    # vector [n_samples, n_features=1] as required for normalisation
    actual_stock_price = dataset_testing.iloc[:, 1:2].values
    print('\nTest (actual) open stock price data set:')
    print(actual_stock_price)
    # Concatenate the data because we need n_features previous instances in
    # order to get the stock price for each day, therefore we need both
    # training and test data
    total_data = pd.concat((dataset_training['Open'], dataset_testing['Open']),
                           axis=0)
    # Reshape and scale the input features matrix to prepare the test data.  We
    # are predicting pred_points, the length of the test labels vector, so in
    # order to prepare the test set we take the lower bound value as n_features
    # and the upper bound value as n_features + pred_points; this ensures that
    # the difference pred_points is maintained.
    inputs = total_data[
        len(total_data) - len(dataset_testing) - n_features:].values
    inputs = inputs.reshape(-1, 1)
    inputs = scaler.transform(inputs)
    X_test = []
    # Iterate through the test data set and produce test feature matrix
    pred_points = actual_stock_price.shape[0]
    for i in range(n_features, n_features + pred_points):
        X_test.append(inputs[i - n_features:i, 0])
    # Convert list of arrays to NumPy array
    X_test = np.array(X_test)
    # Reshape the test feature matrix into 3D tensor required by RNN model
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
    return X_test, actual_stock_price


def visualise(actual, predicted, title=None):
    """Plot the actual and predicted data.

    This function plots the actual and predicted data together on the same
    plot.
    :param array actual: Actual data.
    :param array predicted: Predicted data.
    :param string title: Plot title.
    """
    plt.plot(actual,
             color='green',
             label='Actual Opening Stock Price',
             ls='--')
    plt.plot(predicted,
             color='red',
             label='Predicted Opening Stock Price',
             ls='-')
    plt.title(title)
    plt.xlabel('Time (days)')
    plt.ylabel('Opening Stock Price')
    plt.legend()
    plt.show()
