"""
Predict the trend of Microsoft's stock price using an RNN architecture built
 with LSTM with 50 units (Neurons) and dropout regularisation. This module
 examines the stock price of Microsoft over a period of 5 years; from January 1
 2014 to December 31, 2018.  The aim is to try to predict and forecast the
 future trend for January 2019 using RNNs.  The actual values for January 2019
 are available to enable comparison with the predicted values.  Predictions
 will be made on the open stock price.
"""

from sklearn.preprocessing import MinMaxScaler

import stock_utils


def main():
    # Data set scaler for normalising the data
    scaler = MinMaxScaler(feature_range=(0, 1))

    # Load training data set
    dataset_training, training_data_scaled = \
        stock_utils.load_training_data(training_data_file='MSFT_train.csv',
                                       scaler=scaler)

    # Create the data to get 60 timestamps from the current instance.  This
    # provide sufficient number of previous instances in order to understand
    # the trend.
    X_train, y_train = \
        stock_utils.training_features_labels(training_data_scaled,
                                             n_features=60)

    # Load or build and fit model (with dropout regularisation)
    model = stock_utils.rnn_model(X_train, y_train, num_units=50,
                                  num_epochs=100, batch_size=32,
                                  model_file='msft_lstm50do.h5', dropout=0.2)

    # Prepare test data (test features and test labels)
    X_test, actual_stock_price = \
        stock_utils.test_features_labels(test_data_file='MSFT_test.csv',
                                         dataset_training=dataset_training,
                                         scaler=scaler, n_features=60)

    # Test model predictions using test data as input
    predicted_stock_price = model.predict(X_test)
    # Get back actual stock prices from scaled results
    predicted_stock_price = scaler.inverse_transform(predicted_stock_price)

    # Visualise the results
    stock_utils.visualise(actual_stock_price, predicted_stock_price,
                          title='Predicted Microsoft Opening Stock Price')


if __name__ == "__main__":
    main()
