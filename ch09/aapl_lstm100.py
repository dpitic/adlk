"""
Predict the trend of Apple's stock price using an LSTM with 100 units
(Neurons).  This module examines the stock price of Apple over a period of 5
years; from January 1, 2014 to December 31, 2018.  The aim is to try to predict
and forecast the future trend for January 2019 using RNNs.  The actual values
for January 2019 are available to enable comparison with the predicted values.
Predictions will be made on the open stock price.
"""

from sklearn.preprocessing import MinMaxScaler

import stock_utils


def main():
    # Data set scaler for normalising the data
    scaler = MinMaxScaler(feature_range=(0, 1))

    # Load training data set
    dataset_training, training_data_scaled = \
        stock_utils.load_training_data(training_data_file='AAPL_train.csv',
                                       scaler=scaler)

    # Create the data to get 60 timestamps from the current instance.  This
    # provide sufficient number of previous instances in order to understand
    # the trend.
    X_train, y_train = \
        stock_utils.training_features_labels(training_data_scaled,
                                             n_features=60)

    # Load or build and fit model
    model = stock_utils.rnn_model(X_train, y_train, num_units=100,
                                  num_epochs=100, batch_size=32,
                                  model_file='aapl_lstm100.h5')

    # Prepare test data (test features and test labels)
    X_test, actual_stock_price = \
        stock_utils.test_features_labels(test_data_file='AAPL_test.csv',
                                         dataset_training=dataset_training,
                                         scaler=scaler, n_features=60)

    # Test model predictions using test data as input
    predicted_stock_price = model.predict(X_test)
    # Get back actual stock prices from scaled results
    predicted_stock_price = scaler.inverse_transform(predicted_stock_price)

    # Visualise the results
    stock_utils.visualise(actual_stock_price, predicted_stock_price,
                          title='Predicted Apple Opening Stock Price')


if __name__ == "__main__":
    main()
