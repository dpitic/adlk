"""Bank machine learning logistic regression model using scikit-learn.  The
goal is to classify the customers in the bank data set into two categories:
those that subscribed to the product, and those that did not subscribe."""

import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split


def main():
    # Load the data set
    features_file = 'bank/bank_data_feats_e3.csv'
    feats = pd.read_csv(features_file, index_col=0)
    target_file = 'bank/bank_data_target_e2.csv'
    target = pd.read_csv(target_file, index_col=0)

    # Create the training and test data set, using 20% for test
    test_size = 0.2
    random_state = 42
    X_train, X_test, y_train, y_test = train_test_split(
        feats, target, test_size=test_size, random_state=random_state)
    # Check dimensions are correct
    print(f'Shape of X_train: {X_train.shape}')
    print(f'Shape of y_train: {y_train.shape}')
    print(f'Shape of X_test:  {X_test.shape}')
    print(f'Shape of y_test:  {y_test.shape}')

    # Instantiate and train the logistic regression model
    model = LogisticRegression(random_state=random_state, solver='liblinear')
    model.fit(X_train, np.ravel(y_train))

    # Test the model by predicting the outcome on the test data
    y_pred = model.predict(X_test)
    # Compare prediction accuracy using true values (y_test); accuracy is
    # defined as the proportion of predicted values that equal true values
    accuracy = metrics.accuracy_score(y_true=y_test, y_pred=y_pred)
    print(f'\nAccuracy of the model is {accuracy * 100:.4f}%')

    # Other common evaluation metrics for classification models
    precision, recall, fscore, _ = metrics.precision_recall_fscore_support(
        y_true=y_test, y_pred=y_pred, average='binary')
    print(f'\nPrecision: {precision:.4f}\nRecall: {recall:.4f}\n'
          f'fscore: {fscore:.4f}')

    # Model coefficients; see which features have greatest impact on result
    print('\nModel coefficients:')
    coef_list = [f'{feature}: {coef}' for coef, feature in
                 sorted(zip(model.coef_[0], X_train.columns.values.tolist()))]
    for item in coef_list:
        print(item)

    # Model baseline.  Determine the percentage of each target value
    print('\nProportion of each target value:')
    print(target['y'].value_counts() / target.shape[0] * 100)
    # Use the '0' prediction as the baseline
    y_baseline = pd.Series([0] * target.shape[0])
    # Check performance metrics on baseline model
    precision, recall, fscore, _ = metrics.precision_recall_fscore_support(
        y_true=target['y'], y_pred=y_baseline, average='macro')
    print(f'\nPrecision: {precision:.4f}\nRecall: {recall:.4f}\n'
          f'fscore: {fscore:.4f}')


if __name__ == '__main__':
    main()
