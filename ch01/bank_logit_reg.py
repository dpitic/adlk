"""Logistic regression with regularisation and search for optimum
regularisation parameter (hyperparameter tuning)."""

import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn.linear_model import LogisticRegressionCV
from sklearn.model_selection import train_test_split


def main():
    # Load the features and target data
    feats = pd.read_csv('bank/bank_data_feats_e3.csv', index_col=0)
    target = pd.read_csv('bank/bank_data_target_e2.csv', index_col=0)

    # Create training and test data set, using 20% for test
    test_size = 0.2
    random_state = 13
    X_train, X_test, y_train, y_test = train_test_split(
        feats, target, test_size=test_size, random_state=random_state)
    # Check dimensions are correct
    print(f'Shape of X_train: {X_train.shape}')
    print(f'Shape of y_train: {y_train.shape}')
    print(f'Shape of X_test:  {X_test.shape}')
    print(f'Shape of y_test:  {y_test.shape}')

    # Inverse regularisation strength
    Cs = np.logspace(-2, 6, 9)
    # 10 folds for cross-validation (10% of training data set for validation)
    model_l1 = LogisticRegressionCV(Cs=Cs, penalty='l1', cv=10,
                                    solver='liblinear',
                                    random_state=42)
    model_l2 = LogisticRegressionCV(Cs=Cs, penalty='l2', cv=10,
                                    random_state=42)

    model_l1.fit(X_train, y_train['y'])
    model_l2.fit(X_train, y_train['y'])

    # Best hyperparameters are automatically chosen (lowest error)
    print(f'\nBest hyperparameter for l1 regularisation model: '
          f'{model_l1.C_[0]}')
    print(f'Best hyperparameter for l2 regularisation model: '
          f'{model_l2.C_[0]}')

    # Evaluate model using predictions on test data
    y_pred_l1 = model_l1.predict(X_test)
    y_pred_l2 = model_l2.predict(X_test)

    accuracy_l1 = metrics.accuracy_score(y_true=y_test, y_pred=y_pred_l1)
    accuracy_l2 = metrics.accuracy_score(y_true=y_test, y_pred=y_pred_l2)
    print(f'\nAccuracy of the model with l1 regularisation: '
          f'{accuracy_l1 * 100:.4f}%')
    print(f'Accuracy of the model with l2 regularisation: '
          f'{accuracy_l2 * 100:.4f}%')

    # Other evaluation metrics
    precision_l1, recall_l1, fscore_l1, _ = \
        metrics.precision_recall_fscore_support(
            y_true=y_test, y_pred=y_pred_l1, average='binary')
    precision_l2, recall_l2, fscore_l2, _ = \
        metrics.precision_recall_fscore_support(
            y_true=y_test, y_pred=y_pred_l2, average='binary')
    print(f'\nl1\nPrecision: {precision_l1:.4f}\nRecall: {recall_l1:.4f}\n'
          f'fscore: {fscore_l1:.4f}')
    print(f'\nl2\nPrecision: {precision_l2:.4f}\nRecall: {recall_l2:.4f}\n'
          f'fscore: {fscore_l2:.4f}')

    # Feature importance
    print('\nModel coefficients for l1 model:')
    coef_list = [
        f'{feature}: {coef}' for coef, feature in sorted(
            zip(model_l1.coef_[0], X_train.columns.values.tolist()))
    ]
    for item in coef_list:
        print(item)
    print('\nModel coefficients for l2 model:')
    coef_list = [
        f'{feature}: {coef}' for coef, feature in sorted(
            zip(model_l2.coef_[0], X_train.columns.values.tolist()))
    ]
    for item in coef_list:
        print(item)


if __name__ == "__main__":
    main()
