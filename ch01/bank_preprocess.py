"""Bank data machine learning preprocessing."""
import os

import matplotlib.pyplot as plt
import pandas as pd


def save_csv(dataframe, filename=None, overwrite=False, **kwargs):
    """Save the dataframe to the specified CSV filename.

    This is a helper function that writes the DataFrame to a CSV file and
    outputs status messages based on whether the file exists or not.

    :param DataFrame dataframe: DataFrame to save to CSV file.
    :param str filename: Output CSV file name, default=None.
    :param bool overwrite: File overwrite flag, default=False.
    :param dict **kwargs: Arguments for pandas.DataFrame.to_csv()
    """
    if filename is not None:
        # Check if file exists to either overwrite or not
        if os.path.isfile(filename):
            # File exists, only overwrite if intended
            if overwrite is True:
                print(f'Overwriting {filename}')
                dataframe.to_csv(filename, **kwargs)
            else:
                print(f'{filename} exists; not overwriting.')
        else:
            # File does not exist, write to CSV file
            dataframe.to_csv(filename, **kwargs)
            print(f'{filename} saved.')
    else:
        # No filename given; don't save file
        print('File not saved.')


def load_data(filename, features_file=None, targets_file=None):
    """Return the features and targets.

    Load data from the specified data file and return the features and targets
    DataFrame objects.  If features filename and targets filename is provided,
    save the data disk.
    :param str filename: Raw data file.
    :param str features_file: File name to save features DF to.
    :param str targets_file: File name to save targets DF to.
    :return DataFrame features: Features data set.
    :return Series targets: Target values.
    """
    bank_data = pd.read_csv(filename, sep=';')
    print(bank_data.head())
    print(f'\nThere are {bank_data.shape[0]} rows and {bank_data.shape[1]} '
          'columns')
    features = bank_data.drop('y', axis=1)
    targets = bank_data['y']
    print(f'\nFeatures table has {features.shape[0]} rows and '
          f'{features.shape[1]} columns')
    print(f'\nTargets table has {targets.shape[0]} rows')
    print(targets.head())

    # Save the features and targets for later use
    save_csv(features, features_file, overwrite=True)
    save_csv(targets, targets_file, overwrite=True, header=True)
    return features, targets


def clean_features(filename, out_file=None):
    """Clean features file and convert non-numerical data to numerical.
    :param str filename: Features file name.
    :param str out_file: Features output file name (CSV).
    :return DataFrame: Clean features for training.
    """
    bank_data = pd.read_csv(filename, index_col=0)
    print('\nBank data')
    print(bank_data.describe())
    bank_data.hist()
    plt.show()
    # Convert binary columns into numerical columns; rename column as required
    print('\nBank data "defaults"')
    print(bank_data['default'].value_counts())
    bank_data['default'].value_counts().plot(kind='bar')
    plt.title('default')
    plt.show()
    # Convert 'default' column: yes = 1, no = 0; rename to 'is_default'
    bank_data['is_default'] = bank_data['default'].apply(
        lambda row: 1 if row == 'yes' else 0)
    print(bank_data[['default', 'is_default']].tail())
    # Convert 'housing' column
    print('\nBank data "housing"')
    print(bank_data['housing'].value_counts())
    bank_data['housing'].value_counts().plot(kind='bar')
    plt.title('housing')
    plt.show()
    bank_data['is_housing'] = bank_data['housing'].apply(
        lambda row: 1 if row == 'yes' else 0)
    # Convert 'loan' column
    print('\nBank data "loan"')
    print(bank_data['loan'].value_counts())
    bank_data['loan'].value_counts().plot(kind='bar')
    plt.title('loan')
    plt.show()
    bank_data['is_loan'] = bank_data['loan'].apply(
        lambda row: 1 if row == 'yes' else 0)
    # Convert categorical columns using one-hot encoding in Pandas (dummies)
    # Start with 'marital' column
    print('\nBank data "marital"')
    print(bank_data['marital'].value_counts())
    bank_data['marital'].value_counts().plot(kind='bar')
    plt.title('marital')
    plt.show()
    marital_dummies = pd.get_dummies(bank_data['marital'])
    print(pd.concat([bank_data['marital'], marital_dummies], axis=1).head(10))
    # Remove redundant 'divorced' column
    marital_dummies.drop('divorced', axis=1, inplace=True)
    marital_dummies.columns = [f'marital_{colname}' for colname in
                               marital_dummies.columns]
    print('\nBank data "marital" dummies')
    print(marital_dummies.head())
    # Add dummy columns back to original feature data
    bank_data = pd.concat([bank_data, marital_dummies], axis=1)
    # bank_data.drop('marital', axis=1, inplace=True)
    # Repeat for remaining categorical data: education, job, contact, poutcome
    # 'job' column
    print('\nBank data "job"')
    print(bank_data['job'].value_counts())
    bank_data['job'].value_counts().plot(kind='bar')
    plt.title('job')
    plt.show()
    job_dummies = pd.get_dummies(bank_data['job'])
    job_dummies.drop('unknown', axis=1, inplace=True)
    job_dummies.columns = [f'job_{colname}' for colname in job_dummies.columns]
    bank_data = pd.concat([bank_data, job_dummies], axis=1)
    # 'education' column
    print('\nBank data "education"')
    print(bank_data['education'].value_counts())
    bank_data['education'].value_counts().plot(kind='bar')
    plt.title('education')
    plt.show()
    edu_dummies = pd.get_dummies(bank_data['education'])
    edu_dummies.drop('unknown', axis=1, inplace=True)
    edu_dummies.columns = [f'education_{colname}' for colname in
                           edu_dummies.columns]
    bank_data = pd.concat([bank_data, edu_dummies], axis=1)
    # 'contact' column
    print('\nBank data "contact"')
    print(bank_data['contact'].value_counts())
    bank_data['contact'].value_counts().plot(kind='bar')
    plt.title('contact')
    plt.show()
    contact_dummies = pd.get_dummies(bank_data['contact'])
    contact_dummies.drop('unknown', axis=1, inplace=True)
    contact_dummies.columns = [f'contact_{colname}' for colname in
                               contact_dummies.columns]
    bank_data = pd.concat([bank_data, contact_dummies], axis=1)
    # 'month' column is ordinal data; map to month number, start at 1
    month_map = {'jan': 1, 'feb': 2, 'mar': 3, 'apr': 4, 'may': 5, 'jun': 6,
                 'jul': 7, 'aug': 8, 'sep': 9, 'oct': 10, 'nov': 11, 'dec': 12}
    print('\nBank data "month"')
    print(bank_data['month'].value_counts())
    bank_data['month'].value_counts().plot(kind='bar')
    plt.title('month')
    plt.show()
    # Convert the column using the map() function
    bank_data['month'] = bank_data['month'].map(month_map)
    # 'poutcome' column
    print('\nBank data "poutcome"')
    print(bank_data['poutcome'].value_counts())
    bank_data['poutcome'].value_counts().plot(kind='bar')
    plt.title('poutcome')
    plt.show()
    poutcome_dummies = pd.get_dummies(bank_data['poutcome'])
    poutcome_dummies.drop('unknown', axis=1, inplace=True)
    poutcome_dummies.columns = [f'poutcome_{colname}' for colname in
                                poutcome_dummies.columns]
    bank_data = pd.concat([bank_data, poutcome_dummies], axis=1)
    print(bank_data.iloc[0])
    bank_data.drop(['job', 'education', 'marital', 'default', 'housing',
                    'loan', 'contact', 'poutcome'], axis=1, inplace=True)
    print(bank_data.dtypes)
    # Save training features data set
    save_csv(bank_data, out_file, overwrite=False)
    return bank_data


def clean_target(filename, out_file=None):
    """Clean target file."""
    target = pd.read_csv(filename, index_col=0)
    print(target.head())
    print('\nTarget "y"')
    print(target['y'].value_counts())
    target['y'].value_counts().plot(kind='bar')
    plt.title('y')
    plt.show()
    # Convert categorical data
    target['y'] = target['y'].apply(lambda row: 1 if row == 'yes' else 0)
    print(target.head())
    # Save training target data set
    save_csv(target, out_file, overwrite=False)
    return target


def pdays(features=None, features_file=None, out_file=None):
    """Refactor and encode the pdays column.

    Currently if the customer was not contacted, the value is -1, whereas if
    they were contacted the value is the number of days since contact.  There
    are two pieces of information encoded in this one column that can be
    separated:

    * Whether or not they were contacted,
    * If they were contacted, how long ago was that last contact made (days)

    The hypothesis is that if the customer was targeted, they may be more
    likely to subscribe to the product.

    This function converts pdays value of -1 to 0 (not contacted), otherwise
    they have been contacted so the value will be 1.

    Another hypothesis is that the more recently the customer was contacted the
    greater the likelihood they will subscribe.  This may be implemented later.
    :param DataFrame features: Training features, default = None.
    :param str features_file: CSV file of training features, default = None.
    :param str out_file: CSV file to output data.
    :return DataFrame: Training features with encoded pdays.
    """
    # Get the bank data either from the features DataFrame or file
    if features is None and features_file is not None:
        bank_data = pd.read_csv(features_file, index_col=0)
    else:
        bank_data = features
    bank_data['pdays'].hist(bins=50)
    plt.title('pdays')
    plt.show()
    print('\nNumber of rows with -1 value:',
          bank_data[bank_data['pdays'] == -1]['pdays'].count())
    bank_data[bank_data['pdays'] > 0]['pdays'].hist(bins=50)
    plt.title('pdays > 0')
    plt.show()
    # Encode whether the customer was contacted
    bank_data['was_contacted'] = bank_data['pdays'].apply(
        lambda row: 0 if row == -1 else 1)
    bank_data.drop('pdays', axis=1, inplace=True)
    print('\nBank data "was_contacted":')
    print(bank_data['was_contacted'].head())
    save_csv(bank_data, out_file, overwrite=False)
    return bank_data


def main():
    """Script entry point."""
    # Input (raw) data file
    data_file = 'bank/bank.csv'
    # Processed output files
    features_file = 'bank/bank_data_feats.csv'
    targets_file = 'bank/bank_data_target.csv'
    print('\nLoading and processing data raw data ...')
    features, targets = load_data(data_file, features_file, targets_file)

    # Preprocess training features
    training_features_file = 'bank/bank_data_feats_e2.csv'
    print('\nCleaning training features ...')
    bank_data = clean_features(features_file, out_file=training_features_file)

    # Preprocess training targets
    training_target_file = 'bank/bank_data_target_e2.csv'
    print('\nCleaning training target data ...')
    target = clean_target(targets_file, training_target_file)

    # Encode the hypothesis that a customer will be more likely to subscribe to
    # the product that they were previously targeted with, by transforming the
    # pdays column.  Wherever the value is -1, it will be transformed to 0,
    # indicating the customer has never been previously contacted.  Otherwise,
    # the value will be 1.
    training_features_file = 'bank/bank_data_feats_e3.csv'
    feats = pdays(bank_data, out_file=training_features_file)


if __name__ == '__main__':
    main()
