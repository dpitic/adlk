# adlk

Applied Deep Learning with Keras by Retesh Bhagwat, Mahla Abdolahnejad, Matthew
Moocarme, 2019.

## Notes

### Virtual Environment

The ```conda``` virtual environment used for development is ```ml```.  To ensure
compatibility across Linux and macOS and across Emacs, Sublime Text, and Visual
Studio Code, symlink the installed ```conda``` virtual environment ```envs/```
directory to ```${HOME}/.conda/envs/```.